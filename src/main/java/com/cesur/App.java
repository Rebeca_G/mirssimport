package com.cesur;


import java.util.List;
import java.util.Scanner;
import java.io.BufferedReader;

import java.io.FileReader;


public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="MiRssRebeca";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    /** 
     * @param args
     */
    public static void main( String[] args )
    {
     
    
//MENÚ 
bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
Scanner sc = new Scanner(System.in);
        
int numero;
do {
    System.out.println("");
    System.out.println("***¿QUÉ DESEA HACER?***");
    System.out.println("0. Importar RSS ");
    System.out.println("1. ver Titulo 1 post");
    System.out.println("2. ver todos los post");
    System.out.println("3. Ver en detalle un solo post ( indicandole el ID)");
    System.out.println("4. Buscar por titulo o contenido una palabra");
    System.out.println("5. Filtrar por categoria los post ( ver todos los post de una categoria)");
    System.out.println("6. trigger para asignar una categoria por defecto ");
    
    System.out.println("Cualquier otro numero para salir");

    numero = sc.nextInt();
    switch (numero) {
    case 0:
          recorrerRSS(b);
        break;
    case 1:
        mostrarUnPostTitulo(b);
        break;
    case 2:
        mostrarPosts(b);
        break;
    case 3:
        mostrarUnPostDetalle(b);
        break;
    case 4:
        mostrarPorTituloContenido(b);
        break;
    case 5:
        mostrarPorCategoria(b);
        break;
    case 6:
        mostrarTrigger();
        break;
  
    }

} while (numero >= 0 && numero <= 6);
System.out.println("Estás fuera del menú de opciones"); 

}



//RECORRER RSS 
public static void recorrerRSS(bbdd b){

    XmlParse x =new XmlParse();
    String url ="https://lenguajedemarcasybbdd.wordpress.com/feed/";

    List<String>  titulos = x.leer("//item/title/text()",url);
    List <String> categorias= x.leer("//item/category/text()", url);
    List <String> contenido= x.leer("//item/description/text()", url);
    List <String> comentarios= x.leer("//item/comments/text()",url);
    List <String> autores= x.leer("//item/creator/text()",url);
    List <String> fechasPost= x.leer("//item/pubDate/text()",url);
    List <String> fechasComentario= x.leer("//item/slash:comments/text()",url);//

    
    System.out.println(("Total post: " + titulos.size()));
    for (int i=0;i<titulos.size();i++){
        System.out.println("Autor: "+ autores.get(i));
        System.out.println("Titulo: "+titulos.get(i));
        System.out.println( "Comentario: "+comentarios.get(i));
        System.out.println("Categoria: "+categorias.get(i)); 
        System.out.println("Contenido: "+contenido.get(i));
    }

    
   /* System.out.println("");
    System.out.println(("Total categorias: " + categorias.size()));
    for (int i = 0; i < categorias.size(); ++i) {
        System.out.println("Categoria:" + categorias.get(i));
        }

    System.out.println(("Total comentarios: " + comentarios.size()));
    for (int i = 0; i < comentarios.size(); ++i) {
            System.out.println("Comentarios:" + comentarios.get(i));
         }
*/
   
    for (int i =0;i<titulos.size();i++){
       System.out.println(i);
       String insertarPost= "insert into Post (titulo,fechaCreacion,contenido,categoria_id,idUsuario) values('"+titulos.get(i)+"','"+fechasPost.get(i)+"','"+contenido.get(i)+"',(select top(1) id from categoria where nombre='"+categorias.get(i)+"'),(select top(1) id from Usuario where usuario='"+autores.get(i)+"'))";
            String insertarComentario="Insert into Comentario (comentario, post_id, fechaCreacion, idusuario) values ('"+comentarios.get(i)+"','"+i+"',null,(select top(1) id from Usuario where usuario='"+autores.get(i)+"'))";
            b.modificarBBDD("Insert into Categoria (nombre) values('"+categorias.get(i)+"')");
            b.modificarBBDD("Insert into Usuario (usuario) values ('"+autores.get(i)+"') ");
            b.modificarBBDD(insertarPost);
            //b.modificarBBDD(insertarComentario);
     //al insertar comentarios me da error de FK de id de Post y no me deja
    }

}

//METODOS MENU

public static void mostrarUnPostTitulo(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el numero de Post del Titulo que quieres visualizar"); //del 1 al 3 xq solo hay 3
    int id=sc.nextInt();
    b.leerBBDD("select titulo from Post where id="+id+"",1); //quiero q muestre 5 campos de columna
}

public static void mostrarPosts(bbdd b){
    b.leerBBDD("select * from Post",5) ;


}
public static void mostrarUnPostDetalle(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el numero de Post que quieres visualizar"); 
    int id=sc.nextInt();
    b.leerBBDD("select * from Post where id="+id+"",5); 
}

public static void mostrarPorCategoria(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el numero de Categoria que quieres visualizar"); 
    int idCat=sc.nextInt();
    b.leerBBDD("select * from Post where categoria_id="+idCat+"",5); 
}

public static void mostrarPorTituloContenido(bbdd b){
    Scanner sc = new Scanner(System.in);
            
    int numero;
    do {
        System.out.println("");
        System.out.println("***¿QUÉ DESEA HACER?***");
        System.out.println("1. Buscar por Titulo ");
        System.out.println("2. Buscar por Contenido");
            
        System.out.println("Cualquier otro numero para salir");

        numero = sc.nextInt();
        switch (numero) {
        case 1:
            buscarTitulo(b);
            break;
        case 2:
            buscarContenido(b);
            break;  
        }

    } while (numero >= 1 && numero <= 2);
    System.out.println("Estás fuera del menú de opciones"); // meter metodo numeros

    }

public static void buscarTitulo(bbdd b){
        Scanner sc=new Scanner(System.in);
        System.out.println("Introduce el Titulo que deseas buscar"); 
        String titulo=sc.nextLine();
        b.leerBBDD("select titulo from Post where titulo like '%"+titulo+"%'",1); 
 }

 public static void buscarContenido(bbdd b){
        Scanner sc=new Scanner(System.in);
        System.out.println("Introduce el Contenido que deseas buscar"); 
        String contenido=sc.nextLine();
        b.leerBBDD("select titulo from Post where contenido like '%"+contenido+"%'",1); 
}

public static void mostrarTrigger(){
    System.out.println("create trigger asignarCategoria \nOn Post \nfor insert, update \nas \nbegin \ndeclare @catid as int \nset @catid =(select id from Categoria) \nif @catid=null  \n  begin \n  set @catid=1 \n  update Post set categoria_id=@catid \n  end \nend");
}



///////////////////////
    private static void CrearBBDD()
    {
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       // String cero = "0";
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
                  
      
        }
    }



    //PRUEBA DE CREACION DE TABLAS

     public static void CrearTablas(){
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);

        String Categoria= b.leerBBDDUnDato("select COUNT (1) from INFORMATION SCHEMA.TABLES wheere TABLE schema ='dbo' and TABLE NAME ='Categoria'");
        String Post= b.leerBBDDUnDato("select COUNT (1) from INFORMATION SCHEMA.TABLES wheere TABLE schema ='dbo' and TABLE NAME ='Post'");
        String Usuario= b.leerBBDDUnDato("select COUNT (1) from INFORMATION SCHEMA.TABLES wheere TABLE schema ='dbo' and TABLE NAME ='Usuario'");
        String Comentarios= b.leerBBDDUnDato("select COUNT (1) from INFORMATION SCHEMA.TABLES wheere TABLE schema ='dbo' and TABLE NAME ='Comentarios'");

        
        if (Categoria.equals("0")){
            b.modificarBBDD("create table Categoria(id int not null IDENTITY(1,1) primary key nombre varchar(255))");
        }
        if (Post.equals("0")){
            b.modificarBBDD("create table Post(id int not null IDENTITY(1,1) primary key, titulo varchar(255), fechaCreacion datetime, idUsuario int not null, categoria_id int not null, constraint FK_Categoria_id foreign key (categoria_id) references Post(id),  constraint FK_idUsuarioP foreign key (idUsuario) references Usuario(id))");     
        }
        if (Usuario.equals("0")){
            b.modificarBBDD(" CREATE TABLE Usuario (id int IDENTITY(1,1) PRIMARY KEY,nombre Varchar(100) NOT NULL,apellidos Varchar(100) NOT NULL,usuario Varchar(100) NOT NULL, email varchar(200) NOT NULL)");
        }
        if (Comentarios.equals("0")){
            b.modificarBBDD("create table Comentarios(id int not null IDENTITY(1,1) primary key,nombre varchar(255),comentario varchar(255),email varchar(255),post_id int not null,fechaCreacion datetime,idUsuario int not null, constraint FK_post_id foreign key (post_id) references Post(id),constraint FK_idUsuario foreign key (idUsuario) references Usuario(id))");
        }

     }

       //PRUEBA DE INSERCION DE CATEGORIAS
     public static void insertarCategoriaBBDD(){
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
      
        XmlParse x =new XmlParse();
        String url ="https://lenguajedemarcasybbdd.wordpress.com/feed/";
        List<String>  list = x.leer("//category/text()",url); 
        
        list = x.leer("//category/text()",url);

       
        System.out.println("Cantidad de categorias encontradas: "+ list.size());
        for (String string: list){
            System.out.println((string));
            b.modificarBBDD("insert into Categoria (nombre) values ('"+string+"')");
        }



     }









    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }

}

CREATE TABLE Usuario (
id int IDENTITY(1,1) PRIMARY KEY,
usuario Varchar(100) NOT NULL,

)
 
create table Categoria(
    id int not null IDENTITY(1,1) primary key,
    nombre varchar(255)
);
 
create table Post(
    id int not null IDENTITY(1,1) primary key,
    titulo varchar(255),
    fechaCreacion varchar (255),
    idUsuario int not null,
	contenido varchar(max),
    categoria_id int not null,
	constraint FK_Categoria_id foreign key (categoria_id) references Post(id),
 	constraint FK_idUsuarioP foreign key (idUsuario) references Usuario(id)
);


create table Comentario(
    id int not null IDENTITY(1,1) primary key,
    comentario varchar(255),
    post_id int not null,
    fechaCreacion varchar (255),
	idUsuario int not null, 
	constraint FK_post_id foreign key (post_id) references Post(id),
 	constraint FK_idUsuario foreign key (idUsuario) references Usuario(id)
);
 
 


create trigger asignarCategoria
On Post
for insert, update
as
begin

declare @catid as int
set @catid =(select id from Categoria)

if @catid=null
	
	begin
	set @catid=1
	update Post set categoria_id=@catid
	end
end
